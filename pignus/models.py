from django.db import models
from django.dispatch import receiver
from allauth.account.signals import user_signed_up
from django.contrib.auth.models import User
from PIL import Image
from django.utils import timezone
from django.urls import reverse

#####################################################

#Tablas

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    image = models.ImageField(default='static/img/user_default_300x300.png', upload_to ='Uimagenes/')

    def __str__(self):
        return f'{self.user.username} Profile'
        
    def save(self): # delimitar tamanio de imagen para no lagear 
        super().save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


  
class Orden(models.Model):
    folio = models.AutoField(primary_key = True)
    titulo = models.CharField(blank = True , max_length=70)
    autor = models.ForeignKey(User, on_delete = models.CASCADE, related_name = 'cliente')
    fecha = models.DateTimeField(default=timezone.now)
    contenido = models.TextField()
    imagen = models.ImageField(upload_to='Imagenes/')
    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse('profile')


@receiver(user_signed_up)
def create_user_profile(request, user, **kwargs):
    profile = Profile.objects.create(user=user)
    profile.save()
