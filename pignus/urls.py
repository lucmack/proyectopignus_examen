
from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views

from .views import PostCreateView

urlpatterns = [
  path('',views.index,name="index"),
  path('perfil/', views.profile, name='profile'),
  path('somos/',views.somos,name="somos"),
  path('contactenos/',views.contactenos,name="contactenos"),
  path('register/', views.register, name='register'),
  path('administrar/', views.admin, name = 'admin'), 
  path('post/', PostCreateView.as_view(), name='post-create'),
  path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)